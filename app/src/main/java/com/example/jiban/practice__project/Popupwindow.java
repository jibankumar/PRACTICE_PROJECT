package com.example.jiban.practice__project;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Popupwindow extends AppCompatActivity {
Button click;
LinearLayout linearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popupwindow);
        click=(Button)findViewById(R.id.click);
        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup();
            }
        });
    }
    public  void popup()
    {
      final  Dialog dialog=new Dialog(Popupwindow.this);
      dialog.setContentView(R.layout.customlayout);
      dialog.setTitle("Session details");
        TextView textView=(TextView)dialog.findViewById(R.id.textView4);
        textView.setText("Popup dialog box");
        ImageView imageView=(ImageView)dialog.findViewById(R.id.imageView3);
        imageView.setImageResource(R.drawable.android);
        Button button=(Button)dialog.findViewById(R.id.button10);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }
}
