package com.example.jiban.practice__project;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class Dynamic_Button extends AppCompatActivity {
    String[] name={"Ok","Yes","Accept"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic__button);
        Button myButton;
        LinearLayout linearLayout=(LinearLayout)findViewById(R.id.linearLayout);
        for(int i=0;i<3;i++)
        {
            myButton = new Button(this);
            myButton.setText(name[i]);
                ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                linearLayout.addView(myButton, lp);
        }

    }
}
