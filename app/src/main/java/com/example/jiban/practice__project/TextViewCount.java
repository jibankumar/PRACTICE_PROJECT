package com.example.jiban.practice__project;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class TextViewCount extends AppCompatActivity {
    TextView count12;
    EditText editText;
    String val;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_view_count);
        editText=(EditText)findViewById(R.id.editText121);
        count12=(TextView)findViewById(R.id.textView_count);
        button=(Button)findViewById(R.id.button13);
        //String s="Kotlin is now an official language on Android. It's expressive, concise, and powerful. Best of all, it's interoperable with our existing Android languages and runtime.";
       // editText.setText(s);
        int text_Count=editText.getText().toString().length();
        if(text_Count>30)
        {

        }

        editText.addTextChangedListener(new TextWatcher() {
          @Override
          public void beforeTextChanged(CharSequence s, int start, int count, int after) {

          }

          @Override
          public void onTextChanged(CharSequence s, int start, int before, int count) {

              int text_Count=editText.getText().toString().length();

              if(text_Count>=20&& text_Count<=50)
              {
                      int c=50-text_Count;
                      val=c+"/"+"50";
                      count12.setText(val);
                      count12.setVisibility(View.VISIBLE);

              }

          }

          @Override
          public void afterTextChanged(Editable s) {

          }
      });


    }

}


