package com.example.jiban.practice__project;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import static java.security.AccessController.getContext;

public class CustomDialogboxExample extends AppCompatActivity {
TextView textView;
Button click;
Context context;
String[]text={"Jiban","Shubham","Aditya","Gyana"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_dialogbox_example);
        click=(Button)findViewById(R.id.button11);
        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showdialog();
            }
        });

    }
    public void showdialog(){
        final Dialog dialog=new Dialog(CustomDialogboxExample.this);
        dialog.setContentView(R.layout.customlinearlayout);
        LinearLayout linearLayout=(LinearLayout)dialog.findViewById(R.id.linearLayout);
        for (int i=0;i<=3;i++)
        {

            LayoutInflater inflater=(LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view=inflater.inflate(R.layout.layoutcustom,null);
            TextView textView=(TextView)view.findViewById(R.id.textView6);
            textView.setText(text[i]);
            linearLayout.addView(view);
        }
        dialog.show();
    }

}

