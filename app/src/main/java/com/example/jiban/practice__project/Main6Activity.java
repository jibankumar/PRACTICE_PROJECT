package com.example.jiban.practice__project;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.example.jiban.practice__project.DatabaseHandler.DATAF_ORMAT;

public class Main6Activity extends AppCompatActivity {
    ListView listView;
    SQLiteDatabase sqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);
        ListView listView = (ListView) findViewById(R.id.listView);
        ArrayList arrayList = new ArrayList();
        arrayList.add("15:30");
        arrayList.add("15:32");
        arrayList.add("15:34");
        ArrayList arrayList1=new ArrayList();
        arrayList1.add("jiban");
        arrayList1.add("jagat");
        arrayList1.add("jagan");
        CustomAdapter adapter=new CustomAdapter(this,arrayList,arrayList1);
        listView.setAdapter(adapter);
        DatabaseHandler databaseHandler=new DatabaseHandler(this);
        databaseHandler.getWritableDatabase();
        sqLiteDatabase=Main6Activity.this.openOrCreateDatabase(DatabaseHandler.DATABASE_NAME,MODE_PRIVATE,null);
        String d1="INSERT INTO " + DATAF_ORMAT+ " Values('jiban','15:30')";
        sqLiteDatabase.execSQL(d1);
        String d2="INSERT INTO " + DATAF_ORMAT+ " Values('jagat','15:32')";
        sqLiteDatabase.execSQL(d2);
        String d3="INSERT INTO " + DATAF_ORMAT+ " Values('jagan','15:34')";
        sqLiteDatabase.execSQL(d3);



        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Calendar cal=Calendar.getInstance();
                int hour=cal.get(Calendar.HOUR);
                int minute=cal.get(Calendar.MINUTE);
                int milisec=cal.get(Calendar.MILLISECOND);
                String time=hour+":"+minute;
                Cursor cursor=sqLiteDatabase.rawQuery("SELECT * FROM "+DatabaseHandler.DATAF_ORMAT,null);
                if(cursor.getCount()>0)
                {
                  if( cursor.moveToFirst())
                  {
                      do {
                          String date=cursor.getString(0);
                          if(date==time)
                          {
                              format();
                          }

                      }while (cursor.moveToNext());

                  }

                }

            }
        }, 60000);
    }
    public void format()
    {
       // Cursor cursor=sqLiteDatabase.
    }
}
class CustomAdapter extends BaseAdapter
{
    Context context;
    ArrayList arrayList;
    ArrayList arrayList1;
    public  CustomAdapter(Context context,ArrayList arrayList,ArrayList arrayList1)
    {
        this.context=context;
        this.arrayList=arrayList;
        this.arrayList1=arrayList1;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList1.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            LayoutInflater inflater=LayoutInflater.from(parent.getContext());
            convertView =inflater.inflate(R.layout.arraylistadapter,parent,false);
        }
        TextView name=(TextView)convertView.findViewById(R.id.textView7);
        TextView time=(TextView)convertView.findViewById(R.id.textView12);
        name.setText(arrayList.get(position).toString());
        time.setText(arrayList1.get(position).toString());
        return convertView;
    }
}
