package com.example.jiban.practice__project;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AlarmTest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_test);
        String dt = "21-04-2018 12:12:00";
        SimpleDateFormat mdformat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        try
        {
            Intent intent = new Intent(getBaseContext(), MyBroadcastReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    getBaseContext(), 0, intent, 0);
            Date dpDate = mdformat.parse(dt);
            int year = dpDate.getYear();
            int month = dpDate.getMonth();
            int day = dpDate.getDay();
            int hour = dpDate.getHours();
            int minute = dpDate.getMinutes();
            Calendar calendar1 = Calendar.getInstance();
            long p =   calendar1.getTimeInMillis();
            GregorianCalendar calendar = new GregorianCalendar(year, month, day, hour, minute);
            long alarm_time = calendar.getTimeInMillis();
            android.app.AlarmManager alarmManager = (android.app.AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, alarm_time, pendingIntent);
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
