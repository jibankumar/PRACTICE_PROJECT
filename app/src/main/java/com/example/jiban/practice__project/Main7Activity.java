package com.example.jiban.practice__project;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class Main7Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main7);
        Button click=(Button)findViewById(R.id.click);
        RelativeLayout linearLayout=(RelativeLayout) findViewById(R.id.linear);
        //click.setDuplicateParentStateEnabled(true);
      /* linearLayout.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {
               return true;
           }
       });*/
        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Jiban",Toast.LENGTH_LONG).show();
            }
        });
    }
}
