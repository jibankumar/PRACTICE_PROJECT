package com.example.jiban.practice__project;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;


public class BlankFragment extends Fragment {
    Snackbar snackbar;
    FrameLayout frameLayout;
    Button btn;
    View view;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        frameLayout=(FrameLayout)view.findViewById(R.id.frame);
        btn=(Button)view.findViewById(R.id.button6);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar=Snackbar.make(getView(),"Snackbare",Snackbar.LENGTH_LONG);
                View snackbarView=snackbar.getView();
                snackbarView.setBackgroundColor(Color.GREEN);
                snackbar.show();
            }
        });

       View view=inflater.inflate(R.layout.fragment_blank, container, false);
        return view;
    }

    }




