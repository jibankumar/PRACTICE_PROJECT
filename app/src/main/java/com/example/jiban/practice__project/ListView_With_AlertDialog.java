package com.example.jiban.practice__project;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import static java.lang.String.valueOf;

public class ListView_With_AlertDialog extends AppCompatActivity {
    Button click;
    ArrayList<String> name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view__with__alert_dialog);
        click=(Button)findViewById(R.id.button2);
        name=new ArrayList<String>();
        name.add("Aditya");
        name.add("Gyana");
        name.add("Soumya");
        name.add("jiban");
        name.add("Manoranjan");
        name.add("Pratikshya");
        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listViewDialogbox();
            }
        });

    }
    public  void listViewDialogbox()
    {

        final CharSequence[] Name=name.toArray(new String[name.size()]);
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Name");
        builder.setCancelable(false);
        builder.setItems(Name,new  DialogInterface.OnClickListener()
        {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                String selectname=Name[item].toString();
                Toast.makeText(getApplicationContext(),valueOf(selectname),Toast.LENGTH_LONG).show();
            }
        });
        AlertDialog alertDialog=builder.create();
        alertDialog.show();


    }
}
