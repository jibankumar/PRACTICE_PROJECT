package com.example.jiban.practice__project;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CountableEditText extends AppCompatActivity {
    EditText editText;
    Button button;
    int text_conut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countable_edit_text);
        editText = (EditText) findViewById(R.id.count_text);
        String s = "Specifying the Input Method Type. This";
        editText.setText(s);
        button = (Button) findViewById(R.id.button_click);
       // button.setEnabled(false);
        text_conut = editText.getText().toString().length();
        if(text_conut>30)
        {
            button.setEnabled(false);
            //editText.setTextColor(Color.BLUE);
            //editText.setBackgroundColor(Color.WHITE);
        }else {
            button.setEnabled(true);
           // editText.setTextColor(Color.BLACK);
        }



        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                text_conut = editText.getText().toString().length();
                if (text_conut > 30) {
                    button.setEnabled(false);
                    button.setBackgroundColor(Color.BLACK);
                   /* editText.setTextColor(Color.BLUE);
                    editText.setBackgroundColor(Color.WHITE);*/

                    //  Toast.makeText(getApplicationContext(), "false", Toast.LENGTH_SHORT).show();

                } else {
                    button.setEnabled(true);
                    button.setBackgroundColor(Color.BLUE);
                    //editText.setTextColor(Color.BLACK);
                    //  Toast.makeText(getApplicationContext(), "True", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }
}

