package com.example.jiban.practice__project;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static android.content.Context.*;

public class SharedPreferences_Example extends AppCompatActivity {
    EditText name,email,password,phone;
    Button signup;
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preferences__example);
        name=(EditText)findViewById(R.id.editText3);
        email=(EditText)findViewById(R.id.editText4);
        password=(EditText)findViewById(R.id.editText5);
        phone=(EditText)findViewById(R.id.editText6);
        signup=(Button)findViewById(R.id.button5);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                share();
            }
        });
    }
    public void share()

    {
        preferences=getSharedPreferences("MyPREFERENCES",Context.MODE_PRIVATE);
        editor.putString("Name",name.getText().toString());
        editor.putString("Email",email.getText().toString());
        editor.putString("Password",password.getText().toString());
        editor.putString("Phone",phone.getText().toString());
        editor.commit();

        Intent intent=new Intent(SharedPreferences_Example.this,Signinpage.class);
        startActivity(intent);


    }

}
