package com.example.jiban.practice__project;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.concurrent.TimeUnit;

public class ShareAppActivity extends AppCompatActivity {
    CommonUtilities commonUtilities;
    ProgressDialog progressDialog;
    String receivedText;
    TextView textView;
    Button sendButton;
    Snackbar snackbar;
    ConstraintLayout constraintLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_app);
        constraintLayout=(ConstraintLayout)findViewById(R.id.constraintLayout);
        textView=(TextView)findViewById(R.id.text);
        sendButton=(Button)findViewById(R.id.btn);
        Intent receivedIntent = getIntent();
        String receivedAction = receivedIntent.getAction();
        String receivedType = receivedIntent.getType();
        if(receivedAction.equals(Intent.ACTION_SEND)){

        }
        else if(receivedAction.equals(Intent.ACTION_MAIN)){

        }
        textView.setText("Nothing has been shared!");

        if(receivedType.startsWith("text/")){
            //handle sent text
            receivedText = receivedIntent.getStringExtra(Intent.EXTRA_TEXT);

         /* if(receivedText!=null){
              try {
                  sendRequestToServer();
              } catch (JSONException e) {
                  e.printStackTrace();
              }
          }*/
        }
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    sendRequestToServer();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }
    private void sendRequestToServer() throws JSONException {
        progressDialog = ProgressDialog.show(ShareAppActivity.this,"","please wait");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("text",receivedText);



        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,CommonUtilities.LOCAL,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            String status = response.getString("status");
                            if (status.contains("true")) {
                                Toast.makeText(ShareAppActivity.this, "Success", Toast.LENGTH_SHORT).show();
                             /*  Snackbar snackbar=Snackbar.make(constraintLayout,"Submit Successfully",Snackbar.LENGTH_LONG);
                               View snackbarView=snackbar.getView();
                               snackbarView.setBackgroundColor(Color.BLACK);
                                snackbar.show();*/
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        commonUtilities.showVolleyErrorMessage(getApplicationContext(), error, getResources().getString(R.string.network_unavailable));
                    }
                }) {
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                commonUtilities.showNewtworkResponse(response);
                return super.parseNetworkResponse(response);
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(50),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "TEST");

    }
    }


